package com.example.demo.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class StudentDto {

	private String firstName;
	private String lastName;
	private String email;

}
