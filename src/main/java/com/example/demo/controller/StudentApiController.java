package com.example.demo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Student;
import com.example.demo.service.StudentService;

import lombok.extern.slf4j.Slf4j;



@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
@Slf4j
public class StudentApiController {
	
	@Autowired
	private StudentService studentService;

	@GetMapping("/students")
	public ResponseEntity<List<Student>> students(){		
		List<Student> students = studentService.findAll();
		return ResponseEntity.ok(students);
	
	}
	
	@GetMapping("/students/{id}")
	public ResponseEntity<Student> getStudentById(@PathVariable Long id, Model model){
		Student student = studentService.findOne(id).orElseThrow(() -> new IllegalStateException("Id not found:"+id));
		return ResponseEntity.ok(student);
		
	}
	
	@PostMapping("/student")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Student> edit(@Valid @RequestBody Student student, BindingResult result)  {

		if (result.hasErrors()) {
			 List<ObjectError> allErrors = result.getAllErrors();
			 log.info("errors {} " , allErrors);
			 
		}
		student = studentService.save(student);
		return ResponseEntity.ok(student);
	}	
	
	@PutMapping("/students/{id}")
	public ResponseEntity<Student> editStudent(@PathVariable Long id,@RequestBody Student student){	
		
		studentService.findOne(id).orElseThrow(() -> new IllegalStateException("Id Not Found:"+id));		
		student.setId(id);
		studentService.save(student);
		return ResponseEntity.ok(student);
		
	}
	
	@DeleteMapping("/students/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Student> deleteStudent(@PathVariable Long id){
		studentService.findOne(id).orElseThrow(() -> new IllegalStateException("Id Not Found:"+id));
		studentService.delete(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		
	}	
}
